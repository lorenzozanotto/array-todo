//
//  FolderModel.swift
//  Array
//
//  Created by Lorenzo Zanotto on 31/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//
//
//import Foundation
//
//class FolderModel {
//    
//    var name: String!
//    var itemsCounter: Int!
//    var taskArray: [String]!
//    var folderKey: String!
//    
//    init(key: String, dictionary: Dictionary<String, AnyObject>) {
//        self.folderKey = key
//        
//        // Extracting the name from the dictionary
//        if let name = dictionary["name"] as? String {
//            self.name = name
//        }
//        
//        // Extracting the itemCount from the dictionary
//        if let itemsCounter = dictionary["itemCounter"] as? Int {
//            self.itemsCounter = itemsCounter
//        }
//        
//        // Extracting the contained tasks from the dictionary
//        if let taskArray = dictionary["taskArray"] as? Dictionary<Int, String> {
//            if let arrayItem = taskArray[0]! as? String {
//                self.taskArray.append(arrayItem)
//                print(arrayItem)
//            }
//        }
//        
//    }
//    
//}