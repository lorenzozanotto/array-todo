//
//  FolderCell.swift
//  Array
//
//  Created by Lorenzo Zanotto on 30/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit

class FolderCell: UITableViewCell {

    @IBOutlet weak var titleName: UILabel!
    
    var task: TaskModel!
    
    func configureCell(task: TaskModel) {
        self.titleName.text = task.taskName
    }
    
}
