//
//  Constants.swift
//  Array
//
//  Created by Lorenzo Zanotto on 28/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import Foundation

let KEY_UID = "uid"
let SEGUE_LOGGEDIN = "segueLoggedIn"
let STATUS_ACCOUNT_DOESNT_EXIST = -8