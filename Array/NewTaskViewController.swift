//
//  NewTaskViewController.swift
//  Array
//
//  Created by Lorenzo Zanotto on 29/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import Firebase

class NewTaskViewController: UIViewController {
    
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var switchFolder: UISegmentedControl!
    
    var taskArray = [TaskModel]()
    
    @IBAction func addButtonPressed(sender: AnyObject) {
        
        let taskName = taskTextField.text
        
        var taskType: String {
            if switchFolder.selectedSegmentIndex == 0 {
                return "task"
            } else if switchFolder.selectedSegmentIndex == 1 {
                return "folder"
            } else {
                return ""
            }
        }
        
        let task: Dictionary<String, AnyObject> = [
            "description": taskName!,
            "dateCreated": determineDate(),
            "completed": "non completato",
            "taskNumber": (taskArray.count) + 1,
            "type": taskType
        ]
        
        // Posting the task in the Firebase model
        let firebaseTask = DataService.ds.TASK_BASE.childByAutoId()
        firebaseTask.setValue(task)
        
        //Dismissing the view controller
        dismissViewControllerAnimated(true, completion: nil)

    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Task decoration methods
    func determineDate() -> String {
        
        let nowDate = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
        let formattedStringDate = dateFormatter.stringFromDate(nowDate)
        return formattedStringDate
        
    }

}



