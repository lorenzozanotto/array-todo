//
//  TaskCell.swift
//  Array
//
//  Created by Lorenzo Zanotto on 28/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
    
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var completedImage: UIImageView!
    
    var task: TaskModel!
    
    func configureCell(task: TaskModel) {
        self.titleName.text = task.taskName
        
        // Styling the cell for completed / not completed status
        if task.isCompleted == "completato" && task.type == "task" {
            self.completedImage.image = UIImage(named: "empty")
            self.titleName.textColor = UIColor.lightGrayColor()
        } else if task.isCompleted == "non completato" && task.type == "task" {
            self.completedImage.image = UIImage(named: "indicator")
            self.titleName.textColor = UIColor.blackColor()
        } else {
            // Do nothing
        }
        
        // Styling the cell for task / folder status
        if task.type == "task" {
            // Do nothing
        } else {
            self.titleName.textColor = UIColor.lightTextColor()
            self.completedImage.image = UIImage(named: "empty")
            self.backgroundColor = UIColor.darkGrayColor()
        }
    }
    
    func toggleSelectedStatus(taskCompleted: Bool) -> String {
        
        if taskCompleted == true {
            
            // The task is NOT completed
            completedImage.image = UIImage(named: "indicator")
            titleName.textColor = UIColor.blackColor()
            return "non completato"
        } else {
            
            // The task is completed
            completedImage.image = UIImage(named: "empty")
            titleName.textColor = UIColor.lightGrayColor()
            return "completato"
        }
        
    }
    

}
