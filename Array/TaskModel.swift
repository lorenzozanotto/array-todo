//
//  TaskModel.swift
//  Array
//
//  Created by Lorenzo Zanotto on 28/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import Firebase

class TaskModel {
    
    var taskName: String!
    var taskDate: String!
    var isCompleted: String!
    var taskNumber: Int!
    var taskKey: String!
    var type: String!
    
    var tasksArray = [TaskModel]()
    var task: TaskModel!
    
    class var sharedInstance: TaskModel {
        
        struct Singleton {
            static let instance = TaskModel(name: "", date: "", isCompleted: "")
        }
        
        return Singleton.instance
        
    }
    
    init(name: String, date: String, isCompleted: String) {
        self.taskName = name
        self.taskDate = date
        self.isCompleted = isCompleted
    }
    
    init(taskKey: String, dictionary: Dictionary<String, AnyObject>) {
        self.taskKey = taskKey
        
        // Extracting the description from the dictionary
        if let description = dictionary["description"] as? String {
            self.taskName = description
        }
        
        // Extracting the date from the dictionary
        if let date = dictionary["dateCreated"] as? String {
            self.taskDate = date
        }
        
        // Extracting the isComplete string from the dictionary
        if let completionState = dictionary["completed"] as? String {
            self.isCompleted = completionState
        }
        
        // Extracting the task number field from the dictionary
        if let taskNumber = dictionary["taskNumber"] as? Int {
            self.taskNumber = taskNumber
            print("TASK NUMBER: \(taskNumber)")
        }
        
        // Extracting the cell type field from the dictionary
        if let type = dictionary["type"] as? String {
            self.type = type
            print("TASK NUMBER: \(taskNumber)")
        }
        
        print("Task name: \(self.taskName)")
    }
    
    func syncronizeTaskSnapshot(){
        
        // Syncronizing the Firebase TASKS snapshot
        DataService.ds.TASK_BASE.queryOrderedByChild("taskNumber").observeEventType(.Value, withBlock: { snapshot in
            
            print(snapshot.value)
            
            // Grabbing the snapshot value
            if let snapshots = snapshot.children.allObjects as? [FDataSnapshot] {
                
                // Refreshing the tasks array to prevent appending existing items
                self.tasksArray = []
                
                for snap in snapshots {
                    print("SNAP: \(snap)")
                    
                    if let taskDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        let singleTask = TaskModel(taskKey: key, dictionary: taskDict)
                        self.tasksArray.append(singleTask)
                    }
                }
                
            }
            
        })
        
    }
    
    func getTasks() -> [TaskModel] {
        print("There are \(self.tasksArray.count) elements")
        return self.tasksArray
    }
    
}
