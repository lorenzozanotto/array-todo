//
//  DataService.swift
//  Array
//
//  Created by Lorenzo Zanotto on 28/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import Firebase

let BASE_URL = "https://array-todo.firebaseio.com"

class DataService {
    
    static let ds = DataService()
    
    var REF_BASE = Firebase(url: "\(BASE_URL)")
    var TASK_BASE = Firebase(url: "\(BASE_URL)/tasks")
    var USER_BASE = Firebase(url: "\(BASE_URL)/users")
    var FOLDER_BASE = Firebase(url: "\(BASE_URL)/folders")
    
    // Mark: - User enabled methods
    func createFirebaseUser(uid: String, user: Dictionary<String,String>) {
        USER_BASE.childByAppendingPath(uid).setValue(user)
    }
    
}
