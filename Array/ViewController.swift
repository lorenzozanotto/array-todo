//
//  LoginViewController.swift
//  Array
//
//  Created by Lorenzo Zanotto on 28/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
  
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
    var keyUID: String!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Check if the user has already been authenticated
        if NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID) != nil {
            performSegueWithIdentifier(SEGUE_LOGGEDIN, sender: nil)
        }
    }
  
  @IBAction func loginButton(sender: AnyObject) {
    
    let userEmail = emailTextField.text
    let userPassword = passwordTextField.text
    
    if userEmail != "" && userPassword != "" {
      
      DataService.ds.REF_BASE.authUser(userEmail, password: userPassword, withCompletionBlock: {error, authData in
          
          // Some debugging
          if error != nil {
            
            if error.code == STATUS_ACCOUNT_DOESNT_EXIST {
              DataService.ds.REF_BASE.createUser(userEmail, password: userPassword, withValueCompletionBlock: {error, result /* result is a dict */ in
                  
                  if error != nil {
                    
                  } else {
                    // Saving the user into NSUserDefaults and on Firebase
                    NSUserDefaults.standardUserDefaults().setValue(result[KEY_UID], forKey: KEY_UID)
                    print("NSUser Defaults Saved!")
                    
                    DataService.ds.REF_BASE.authUser(userEmail, password: userPassword, withCompletionBlock: {error, authData in
                        
                        let user = ["provider": authData.provider!, "email":userEmail!]
                        DataService.ds.createFirebaseUser(authData.uid, user: user)
                        
                      })
                    
                    // Once the user authentication is done let's perform the login segue
                    self.performSegueWithIdentifier(SEGUE_LOGGEDIN, sender: nil)
                  }
                })
            }
          } else {
            // If we get here no error has been occourred with .authUser(email:password), the user exists
            NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: KEY_UID)
            self.performSegueWithIdentifier(SEGUE_LOGGEDIN, sender: nil)
          }
    
        })
    }
    
  }
  
}
