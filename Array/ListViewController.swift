//
//  ListViewController.swift
//  Array
//
//  Created by Lorenzo Zanotto on 28/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import Firebase

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var tasks = [TaskModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TaskModel.sharedInstance.syncronizeTaskSnapshot()
        tasks = TaskModel.sharedInstance.getTasks()
        self.tableView.reloadData()
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let taskCellIdentifier = "Cell"
        let folderCellIdentifier = "FolderCell"
        let task = tasks[indexPath.row]
        
        /*  We need to know if the cell that it's going to be
         *  displayed is a FolderCell or a TaskCell, here we
         *  switch its identifier to make this selection
         */
        if task.type == "folder" {
            let cell = tableView.dequeueReusableCellWithIdentifier(folderCellIdentifier, forIndexPath: indexPath) as! FolderCell
            cell.configureCell(task)
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(taskCellIdentifier, forIndexPath: indexPath) as! TaskCell
            cell.configureCell(task)
            return cell
        }
        
    }
    
    // Updating the completion status
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var taskCompleted: Bool!
        
        if tasks[indexPath.row].type == "task" {
            
            // If it's a task cell then we can perform status changes
            let standardCell = tableView.cellForRowAtIndexPath(indexPath) as! TaskCell
            
            if standardCell.completedImage.image == UIImage(named: "empty") {
                taskCompleted = true
            } else {
                taskCompleted = false
            }
            
            /* Updating the completion status on Firebase,
             * toggleSelectedStatus() returns the completed or not completed
             * string based on if the cell has been configured successfully
             */
            DataService.ds.TASK_BASE.childByAppendingPath(tasks[indexPath.row].taskKey).updateChildValues(["completed":standardCell.toggleSelectedStatus(taskCompleted)])
        }
    }
    
    // MARK: - TableView Functionalities
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // Required to initiate custom action
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .Normal, title: "Delete") { action, indexPath in
            
            // Removing items from the list
            let taskItem = self.tasks[indexPath.row]
            DataService.ds.TASK_BASE.childByAppendingPath("\(taskItem.taskKey)").removeValue()
            
        }
        
        deleteAction.backgroundColor = UIColor.darkGrayColor()
        return [deleteAction]
        
    }
    
    // MARK: - Segue & Navigation Data Exchange
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "newTaskSegue" {
            if let destinationController = segue.destinationViewController as? NewTaskViewController {
                destinationController.taskArray = tasks
            }
        }
    }

}






